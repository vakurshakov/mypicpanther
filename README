******      PICPANTHER instruction file      ******

Requisites:
---------------------------------------------
1) C++11 capable compiler (e.g. g++ in GCC > 4.7).
2) Make (e.g. GNU make)
3) Eigen3 header files (in /usr/include/eigen3 per default). Tested with versions 3.2.1, 3.2.2.
4) MPI2 implementation (e.g. OpenMPI) providing mpiCC or equivalent, mpiexec or equivalent (for execution).
5) HDF5 library (> 1.8.0), ideally providing h5c++ wrapper around mpiCC (or equivalent).
   The Makefile tries to detect absence of correct h5c++ and uses mpiCC with proper flags instead (/usr/include and /usr/lib per default).

Build instructions:
---------------------------------------------
A simple makefile is provided.
In a properly configured system, running "make" in the source directory should start the build process.
Manual adjustment of the Eigen3 and HDF5 include/library paths in the makefile might be necessary.
The build process will create an executable file called "imp" in the working directory.

Program execution requisites:
---------------------------------------------
1) Properly setup MPI environment (providing mpiexec or equivalent).
2) A complete "config" file in the working directory.
3) Read-write permissions in the working directory, sufficient disk space.

Program execution:
---------------------------------------------
mpiexec (or equivalent) should be called on the "imp" executable file.
Proper options for the number of MPI processes (matching the number provided in "config") need to be provided (depending on the MPI implementation).
Example (OpenMPI), 4 MPI processes:
	mpiexec -np 4 $BUILD_DIRECTORY/imp
Where $BUILD_DIRECTORY is the directory in which the executable resides.
Simulation output will be written to the working directory, diagnostic messages to stdout (terminal).

Post-processing requisites:
---------------------------------------------
1) Python2 or Python3
2) PyTables for Python 2/3
3) NumPy for Python 2/3
4) SciPy for Python 2/3
5) Matplotlib for Python 2/3

Execution of Python scripts:
---------------------------------------------
Invoke the python interpreter on the file while in a directory containing the files to process.
Example:
	python $SCRIPT_DIRECTORY/disp.py

File descriptions:
---------------------------------------------
C++:
	communicator.h        Header providing Communicator class for MPI communication.
	constants.h           Header providing physical constants.
	hdf.h                 Header providing HDF class for parallel HDF5 output.
	particle.h            Header providing particle class.
	simulation.h          Header providing simulation class (main header, Yee)
	util.h                Header providing misc. utility functions and definitions.

	communicator.cpp      Implementation of MPI communication routines.
	fields.cpp            Implementation of field update step, field interpolation.
	hdf.cpp               Implementation of parallel HDF5 output.
	init.cpp              Implementation of simulation setup routines (replaceable).
	main.cpp              Implementation of main() function (almost empty), immediate hand-off to Simulation object.
	move.cpp              Implementation of particle update loop.
	nk.cpp                Implementation of Newton-Krylov particle movement equation solver.
	operators.cpp         Implementation of sparse matrix operator initialization (Yee).
	output.cpp            Implementation of simple, text-based energy output.
	parameters.cpp        Implementation of config-file parser.
	particle.cpp          Implementation of per-particle calculations (e.g. calculations involving velocity).
	simulation.cpp        Implementation of main simulation state functions, main loop, init routines.
	smooth.cpp            Implementation of scalar field smoothing.
	sources.cpp           Implementation of source term accumulation (charge, current density, pressure, dielectric tensor).

Make:
	Makefile              Build script for make (e.g. GNU make).

Python:
	tools/disp.py         Example post-processing script creating a dispersion relation.
	tools/histo.py        Example post-processing script creating a particle histogram.

Configuration:
	examples              Example configuration files

Replacement files:
	replacement           Replacement files for center-node scheme implementation (currently unsupported).

Input/Output examples:
	example_input-output  Example input and output for test run.
