// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <algorithm>
#include <iostream>
#include <utility>

#include "communicator.h"

using namespace std;

using Eigen::Vector3d;
using Eigen::Vector3i;

//Constructor sets up MPI communication
Communicator::Communicator(int argc, char* argv[], Eigen::Vector3i& global_cell_size_, Eigen::Vector3i& ghost_cells_, Eigen::Vector3i& num_cpus_) :
	num_cpus(num_cpus_),
	global_cell_size(global_cell_size_),
	ghost_cells(ghost_cells_),
	periodic(0, 0, 0),

	outer_min(-ghost_cells),
	sim_min(0, 0, 0)
{

	//create cartesian communicator and get our own rank
	error_state = MPI_Init(&argc, &argv);
	error_state = MPI_Comm_size(MPI_COMM_WORLD, &num_cpus_total);
	error_state = MPI_Dims_create(num_cpus_total, 3, num_cpus.data());
	error_state = MPI_Cart_create(MPI_COMM_WORLD, 3, num_cpus.data(), periodic.data(), 1, &comm);
	error_state = MPI_Comm_rank(comm, &my_rank);

	//get ranks of our nearest neighbors
	for(int dim = 0; dim < 3; dim++) {
		error_state = MPI_Cart_shift(comm, dim, 1, &neighbors[dim][LEFT], &neighbors[dim][RIGHT]);
	}

	//where are we in global space?
	Vector3i dummy1, dummy2;
	error_state = MPI_Cart_get(comm, 3, dummy1.data(), dummy2.data(), cpu_coords.data());

	MPI_Barrier(MPI_COMM_WORLD);

	//since we know where we are, we can distribute the simulation volume amongst ourselves evenly
	//if even division is not possible along an axis, distribute leftovers as evenly as possible
	for(int dim = 0; dim < 3; dim++) {
		sim_size[dim] = global_cell_size[dim] / num_cpus[dim];
		int remaining_cells = global_cell_size[dim] % num_cpus[dim];
		if(remaining_cells > cpu_coords[dim])
			sim_size[dim]++;
		cell_offset[dim] = cpu_coords[dim] * sim_size[dim] + min(cpu_coords[dim], remaining_cells);
	}

	//save our extents for later (output, etc.)
	total_size = sim_size + 2 * ghost_cells;

	inner_min = sim_min  + ghost_cells;
	outer_max = sim_size + ghost_cells;
	sim_max   = sim_size              ;
	inner_max = sim_size - ghost_cells;

	//tell the user about our rank, neighbors, and coordinates in CPU space, one rank after the other
	for(int rank = 0; rank < num_cpus_total; rank++) {
		if(my_rank == rank) {
			cout << "R: " << my_rank;
			cout << " C: " << cpu_coords.transpose();
			cout << " S: " << sim_size.transpose();

			for(int dim = 0; dim < 3; dim++) {
				cout << ", N" << dim << ": " << neighbors[dim][LEFT] << " " << neighbors[dim][RIGHT];
			}
			cout << endl;
			cout.flush();
		}
		MPI_Barrier(MPI_COMM_WORLD);
		cout.flush();
	}
}

//clean up
Communicator::~Communicator() {
	MPI_Finalize();
}

//move fields calculated here to neighboring ghost cells, and vice versa
void Communicator::move_fields(Field& field, int component_size) {
	//one direction after the other, left and right -> top and bottom, front and back
	//we also _want_ to send to ourselves, for periodic boundaries
	for(int dim = 0; dim < 3; dim++) {
		//do we even have a neighbor?
		if(sim_size[dim] == 1)
			continue;

		Vector3i block_size(total_size);
		block_size[dim] = ghost_cells[dim];

		MPI_Request reqs[4];
		int req = 0;

		//dense output and input buffers, left right
		Field out_l(Field::Zero(block_size.prod()*component_size));
		Field in_l (Field::Zero(block_size.prod()*component_size));
		Vector3i start_in_l (outer_min);
		Vector3i end_in_l   (outer_max);
		Vector3i start_out_l(outer_min);
		Vector3i end_out_l  (outer_max);
		Field out_r(Field::Zero(block_size.prod()*component_size));
		Field in_r (Field::Zero(block_size.prod()*component_size));
		Vector3i start_in_r (outer_min);
		Vector3i end_in_r   (outer_max);
		Vector3i start_out_r(outer_min);
		Vector3i end_out_r  (outer_max);

		start_in_l [dim] = outer_min[dim];
		end_in_l   [dim] = sim_min  [dim];

		start_out_l[dim] = sim_min  [dim];
		end_out_l  [dim] = inner_min[dim];

		start_in_r [dim] = sim_max  [dim];
		end_in_r   [dim] = outer_max[dim];

		start_out_r[dim] = inner_max[dim];
		end_out_r  [dim] = sim_max  [dim];

		//put field values from our simulation volume into output buffers through helper functions, left, right
		gather_quantities(field, out_l, start_out_l, end_out_l, component_size);
		gather_quantities(field, out_r, start_out_r, end_out_r, component_size);

		//send first to prevent deadlock
		//send to right neighbor, get from left neighbor, and vice versa
		error_state = MPI_Isend(out_r.data(), block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][RIGHT], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Irecv(in_l.data() , block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][LEFT ], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Isend(out_l.data(), block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][LEFT ], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Irecv(in_r.data() , block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][RIGHT], FIELD_COMM, comm, &reqs[req++]);

		MPI_Waitall(req, reqs, MPI_STATUSES_IGNORE);

		//pull out field values into our borders from input buffers through helper functions, left, right
		if (neighbors[dim][LEFT] != MPI_PROC_NULL){
			scatter_quantities(in_l, field, start_in_l, end_in_l, component_size);
		}
		if (neighbors[dim][RIGHT] != MPI_PROC_NULL){		
			scatter_quantities(in_r, field, start_in_r, end_in_r, component_size);
		}
	}
}

//move source terms accumulated in our ghost cells to neighboring simulation volume, and vice versa
void Communicator::move_sources(Field& field, int component_size) {
	//one direction after the other, left and right -> top and bottom, front and back
	//we also _want_ to send to ourselves, for periodic boundaries
	for(int dim = 0; dim < 3; dim++) {
		//do we even have a neighbor?
		if(sim_size[dim] == 1) continue;

		Vector3i block_size(total_size);
		block_size[dim] = 2*ghost_cells[dim];

		MPI_Request reqs[4];
		int req = 0;

		//dense output and input buffers, left right
		Field out_l(Field::Zero(block_size.prod()*component_size));
		Field in_l (Field::Zero(block_size.prod()*component_size));
		Vector3i start_in_l (outer_min);
		Vector3i end_in_l   (outer_max);
		Vector3i start_out_l(outer_min);
		Vector3i end_out_l  (outer_max);
		Field out_r(Field::Zero(block_size.prod()*component_size));
		Field in_r (Field::Zero(block_size.prod()*component_size));
		Vector3i start_in_r (outer_min);
		Vector3i end_in_r   (outer_max);
		Vector3i start_out_r(outer_min);
		Vector3i end_out_r  (outer_max);

		start_in_l [dim] = outer_min[dim];
		end_in_l   [dim] = inner_min[dim];

		start_out_l[dim] = outer_min[dim];
		end_out_l  [dim] = inner_min[dim];

		start_in_r [dim] = inner_max[dim];
		end_in_r   [dim] = outer_max[dim];

		start_out_r[dim] = inner_max[dim];
		end_out_r  [dim] = outer_max[dim];

		//put source values from our borders into output buffers through helper functions, left, right
		gather_quantities(field, out_l, start_out_l, end_out_l, component_size);
		gather_quantities(field, out_r, start_out_r, end_out_r, component_size);

		//send first to prevent deadlock
		//send to right neighbor, get from left neighbor, and vice versa
		error_state = MPI_Isend(out_r.data(), block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][RIGHT], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Irecv(in_l.data() , block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][LEFT ], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Isend(out_l.data(), block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][LEFT ], FIELD_COMM, comm, &reqs[req++]);
		error_state = MPI_Irecv(in_r.data() , block_size.prod()*component_size, MPI_DOUBLE, neighbors[dim][RIGHT], FIELD_COMM, comm, &reqs[req++]);

		MPI_Waitall(req, reqs, MPI_STATUSES_IGNORE);

		//pull out source values into our simulation volume from input buffers through helper functions, left, right
		if (neighbors[dim][LEFT] != MPI_PROC_NULL){
			scatter_add_quantities(in_l, field, start_in_l, end_in_l, component_size);
		}
		if (neighbors[dim][RIGHT] != MPI_PROC_NULL){
			scatter_add_quantities(in_r, field, start_in_r, end_in_r, component_size);
		}
	}
}

//correct positions for particles we received or moved through periodic boundaries
void Communicator::particle_boundaries(vector<Particle>& parts) const {
	//just do it for every particle we currently have
	for(vector<Particle>::size_type i = 0; i < parts.size(); i++) {
		const Vector3d& r = parts[i].get_r();
		for(int dim = 0; dim < 3; dim++) {
			if(sim_size[dim] == 1) {
				parts[i].set_r(0.5, dim);
				continue;
			}
			if(r(dim) >= sim_size[dim]) {
				parts[i].correct_r(dim, -sim_size[dim]);
			} else if(r(dim) < 0.) {
				parts[i].correct_r(dim, sim_size[dim]);
			}
		}
	}
}

//move particles we are no longer responsible for the the neighbor who is responsible
void Communicator::move_particles(vector<Particle>& parts) {
	//one direction after the other, particles might be moved three times, but that's okay
	//we do _not_ want to send to ourselves, we just correct the particle position in that case
	for(int dim = 0; dim < 3; dim++) {
		//do we even have a neighbor?
		if(sim_size[dim] == 1)
			continue;
		if(neighbors[dim][RIGHT] == my_rank && neighbors[dim][LEFT] == my_rank) //it's us
			continue;

		//input and output buffers
		vector<Particle> incoming_l, incoming_r;
		vector<Particle> outgoing_l, outgoing_r;

		//check if particle left our simulation volume and put it into its respective buffer if it did
		for(vector<Particle>::size_type i = 0; i < parts.size(); i++) {
			if(parts[i].get_r(dim) < 0.) { //to the left
				swap(parts[i], parts.back()); //std::move?
				outgoing_l.push_back(parts.back());
				outgoing_l.back().correct_r(dim, sim_size[dim]);
				parts.pop_back();
				i--;
			} else if(parts[i].get_r(dim) >= sim_size[dim]) { //to the right
				swap(parts[i], parts.back()); //std::move?
				outgoing_r.push_back(parts.back());
				outgoing_r.back().correct_r(dim, -sim_size[dim]);
				parts.pop_back();
				i--;
			}
		}

		//first, send a message: how many particles will be sent / get message how many we receive
		vector<Particle>::size_type out_num_l = outgoing_l.size(), in_num_l = 0;
		vector<Particle>::size_type out_num_r = outgoing_r.size(), in_num_r = 0;

		MPI_Request reqs[4];
		int req = 0;
		//send first to prevent deadlock
		//send to right neighbor, get from left neighbor, and vice versa
		error_state = MPI_Isend(&out_num_r, sizeof(vector<Particle>::size_type), MPI_BYTE, neighbors[dim][RIGHT], PARTICLE_COMM_NUM, comm, &reqs[req++]);
		error_state = MPI_Irecv(&in_num_l , sizeof(vector<Particle>::size_type), MPI_BYTE, neighbors[dim][LEFT ], PARTICLE_COMM_NUM, comm, &reqs[req++]);
		error_state = MPI_Isend(&out_num_l, sizeof(vector<Particle>::size_type), MPI_BYTE, neighbors[dim][LEFT ], PARTICLE_COMM_NUM, comm, &reqs[req++]);
		error_state = MPI_Irecv(&in_num_r , sizeof(vector<Particle>::size_type), MPI_BYTE, neighbors[dim][RIGHT], PARTICLE_COMM_NUM, comm, &reqs[req++]);
		MPI_Waitall(req, reqs, MPI_STATUSES_IGNORE);

		incoming_r.resize(in_num_r);
		incoming_l.resize(in_num_l);

		//send/receive actual particles now
		req = 0;
		//send first to prevent deadlock
		//send to right neighbor, get from left neighbor, and vice versa
		error_state = MPI_Isend(outgoing_r.data(), out_num_r*sizeof(Particle), MPI_BYTE, neighbors[dim][RIGHT], PARTICLE_COMM_DAT, comm, &reqs[req++]);
		error_state = MPI_Irecv(incoming_l.data(), in_num_l *sizeof(Particle), MPI_BYTE, neighbors[dim][LEFT ], PARTICLE_COMM_DAT, comm, &reqs[req++]);
		error_state = MPI_Isend(outgoing_l.data(), out_num_l*sizeof(Particle), MPI_BYTE, neighbors[dim][LEFT ], PARTICLE_COMM_DAT, comm, &reqs[req++]);
		error_state = MPI_Irecv(incoming_r.data(), in_num_r *sizeof(Particle), MPI_BYTE, neighbors[dim][RIGHT], PARTICLE_COMM_DAT, comm, &reqs[req++]);
		MPI_Waitall(req, reqs, MPI_STATUSES_IGNORE);

		//put the ones we get from right/left into our list
		parts.insert(parts.end(), incoming_r.begin(), incoming_r.end());
		parts.insert(parts.end(), incoming_l.begin(), incoming_l.end());
	}
}

//helper function to put data into output buffers
void Communicator::gather_quantities(const Field& src, Field& dest, const Vector3i& start, const Vector3i& end, int component_size) {
	Vector3i block_size = end - start;

	for(int i = start[0]; i < end[0]; i++) {
		for(int j = start[1]; j < end[1]; j++) {
			for(int k = start[2]; k < end[2]; k++) {
				for(int l = 0; l < component_size; l++) {
					dest[ind(i-start[0], j-start[1], k-start[2], l, block_size[0], block_size[1], block_size[2], component_size)] =
						src[indg(i, j, k, l, total_size[0], total_size[1], total_size[2], component_size)];
				}
			}
		}
	}
}

//helper function to get data from input buffers, overwrite old values (needed for field borders)
void Communicator::scatter_quantities(const Field& src, Field& dest, const Vector3i& start, const Vector3i& end, int component_size) {
	Vector3i block_size = end - start;

	for(int i = start[0]; i < end[0]; i++) {
		for(int j = start[1]; j < end[1]; j++) {
			for(int k = start[2]; k < end[2]; k++) {
				for(int l = 0; l < component_size; l++) {
					dest[indg(i, j, k, l, total_size[0], total_size[1], total_size[2], component_size)] =
						src[ind(i-start[0], j-start[1], k-start[2], l, block_size[0], block_size[1], block_size[2], component_size)];
				}
			}
		}
	}
}

//helper function to get data from input buffers, add them to old values (needed for source borders)
void Communicator::scatter_add_quantities(const Field& src, Field& dest, const Vector3i& start, const Vector3i& end, int component_size) {
	Vector3i block_size = end - start;

	for(int i = start[0]; i < end[0]; i++) {
		for(int j = start[1]; j < end[1]; j++) {
			for(int k = start[2]; k < end[2]; k++) {
				for(int l = 0; l < component_size; l++) {
					dest[indg(i, j, k, l, total_size[0], total_size[1], total_size[2], component_size)] +=
						src[ind(i-start[0], j-start[1], k-start[2], l, block_size[0], block_size[1], block_size[2], component_size)];
				}
			}
		}
	}
}
