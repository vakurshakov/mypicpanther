// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

typedef Eigen::Triplet<double> Trip;

//create an identity operator
void Simulation::stencil_identity() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*3);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
	for(int y = -2; y < Ny+2; y++) {
	for(int z = -2; z < Nz+2; z++) {
		//make it work in 1D, 2D as well
		if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
			continue;
		if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
			continue;
		if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
			continue;

		//this is a vector operator
		for(int c = 0; c < 3; c++) {
			int cur = vindg(x, y, z, c);

			trips.push_back(Trip(cur, cur, 1.0));
		}
	}}}
	identity.setFromTriplets(trips.begin(), trips.end());
}

//create a scalar laplace operator
void Simulation::stencil_scalar_laplace() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*7);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
	for(int y = -2; y < Ny+2; y++) {
	for(int z = -2; z < Nz+2; z++) {
		//make it work in 1D, 2D as well
		if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
			continue;
		if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
			continue;
		if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
			continue;

		const int xp = (Nx > 1) ? x+1 : x;
		const int xm = (Nx > 1) ? x-1 : x;
		const int yp = (Ny > 1) ? y+1 : y;
		const int ym = (Ny > 1) ? y-1 : y;
		const int zp = (Nz > 1) ? z+1 : z;
		const int zm = (Nz > 1) ? z-1 : z;

		int cur = sindg(x, y, z);
		trips.push_back(Trip(cur, sindg(x , y , z ), -6./dx/dx));
		trips.push_back(Trip(cur, sindg(xm, y , z ),  1./dx/dx));
		trips.push_back(Trip(cur, sindg(x , ym, z ),  1./dx/dx));
		trips.push_back(Trip(cur, sindg(x , y , zm),  1./dx/dx));
		trips.push_back(Trip(cur, sindg(xp, y , z ),  1./dx/dx));
		trips.push_back(Trip(cur, sindg(x , yp, z ),  1./dx/dx));
		trips.push_back(Trip(cur, sindg(x , y , zp),  1./dx/dx));
	}}}
	scalar_laplace.setFromTriplets(trips.begin(), trips.end());
}

//create a gradient operator (Yee)
void Simulation::stencil_gradient() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*6);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
	for(int y = -2; y < Ny+2; y++) {
	for(int z = -2; z < Nz+2; z++) {
		//make it work in 1D, 2D as well
		if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
			continue;
		if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
			continue;
		if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
			continue;

		const int xp = (Nx > 1) ? x+1 : x;
		const int yp = (Ny > 1) ? y+1 : y;
		const int zp = (Nz > 1) ? z+1 : z;

		int cur = vindg(x, y, z, 0);

		trips.push_back(Trip(cur, sindg(xp, y , z ),  1./dx));
		trips.push_back(Trip(cur, sindg(x , y , z ), -1./dx));

		cur = vindg(x, y, z, 1);

		trips.push_back(Trip(cur, sindg(x , yp, z ),  1./dx));
		trips.push_back(Trip(cur, sindg(x , y , z ), -1./dx));

		cur = vindg(x, y, z, 2);

		trips.push_back(Trip(cur, sindg(x , y , zp),  1./dx));
		trips.push_back(Trip(cur, sindg(x , y , z ), -1./dx));
	}}}
	gradient.setFromTriplets(trips.begin(), trips.end());
}

//create a divergence operator (Yee)
void Simulation::stencil_divergence() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*6);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
		for(int y = -2; y < Ny+2; y++) {
			for(int z = -2; z < Nz+2; z++) {
				//make it work in 1D, 2D as well
				if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
					continue;
				if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
					continue;
				if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
					continue;

				const int xm = (Nx > 1) ? x-1 : x;
				const int ym = (Ny > 1) ? y-1 : y;
				const int zm = (Nz > 1) ? z-1 : z;

				int cur = sindg(x, y, z);

				trips.push_back(Trip(cur, vindg(x , y , z , 0),  1./dx));
				trips.push_back(Trip(cur, vindg(xm, y , z , 0), -1./dx));

				trips.push_back(Trip(cur, vindg(x , y , z , 1),  1./dx));
				trips.push_back(Trip(cur, vindg(x , ym, z , 1), -1./dx));

				trips.push_back(Trip(cur, vindg(x , y , z , 2),  1./dx));
				trips.push_back(Trip(cur, vindg(x , y , zm, 2), -1./dx));
			}
		}
	}

	divergence.setFromTriplets(trips.begin(), trips.end());
}

//create a sparse matrix operator from tensor mu
//make it perform the correct E-field interpolation as well (Yee)
void Simulation::stencil_nu_tensor_op() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*27);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
		for(int y = -2; y < Ny+2; y++) {
			for(int z = -2; z < Nz+2; z++) {
				//make it work in 1D, 2D as well
				if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
					continue;
				if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
					continue;
				if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
					continue;

				const int xp = (Nx > 1) ? x+1 : x;
				const int xm = (Nx > 1) ? x-1 : x;
				const int yp = (Ny > 1) ? y+1 : y;
				const int ym = (Ny > 1) ? y-1 : y;
				const int zp = (Nz > 1) ? z+1 : z;
				const int zm = (Nz > 1) ? z-1 : z;

				int cur = vindg(x, y, z, 0);

				trips.push_back(Trip(cur, vindg(x , y , z , 0), dielectric[tindg(x , y , z , 0)]   ));

				trips.push_back(Trip(cur, vindg(x , y , z , 1), dielectric[tindg(x , y , z , 1)]/4.));
				trips.push_back(Trip(cur, vindg(xp, y , z , 1), dielectric[tindg(x , y , z , 1)]/4.));
				trips.push_back(Trip(cur, vindg(xp, ym, z , 1), dielectric[tindg(x , y , z , 1)]/4.));
				trips.push_back(Trip(cur, vindg(x , ym, z , 1), dielectric[tindg(x , y , z , 1)]/4.));

				trips.push_back(Trip(cur, vindg(x , y , z , 2), dielectric[tindg(x , y , z , 2)]/4.));
				trips.push_back(Trip(cur, vindg(xp, y , z , 2), dielectric[tindg(x , y , z , 2)]/4.));
				trips.push_back(Trip(cur, vindg(xp, y , zm, 2), dielectric[tindg(x , y , z , 2)]/4.));
				trips.push_back(Trip(cur, vindg(x , y , zm, 2), dielectric[tindg(x , y , z , 2)]/4.));

				cur = vindg(x, y, z, 1);

				trips.push_back(Trip(cur, vindg(x , y , z , 0), dielectric[tindg(x , y , z , 3)]/4.));
				trips.push_back(Trip(cur, vindg(x , yp, z , 0), dielectric[tindg(x , y , z , 3)]/4.));
				trips.push_back(Trip(cur, vindg(xm, yp, z , 0), dielectric[tindg(x , y , z , 3)]/4.));
				trips.push_back(Trip(cur, vindg(xm, y , z , 0), dielectric[tindg(x , y , z , 3)]/4.));

				trips.push_back(Trip(cur, vindg(x , y , z , 1), dielectric[tindg(x , y , z , 4)]   ));

				trips.push_back(Trip(cur, vindg(x , y , z , 2), dielectric[tindg(x , y , z , 5)]/4.));
				trips.push_back(Trip(cur, vindg(x , yp, z , 2), dielectric[tindg(x , y , z , 5)]/4.));
				trips.push_back(Trip(cur, vindg(x , yp, zm, 2), dielectric[tindg(x , y , z , 5)]/4.));
				trips.push_back(Trip(cur, vindg(x , y , zm, 2), dielectric[tindg(x , y , z , 5)]/4.));

				cur = vindg(x, y, z, 2);

				trips.push_back(Trip(cur, vindg(x , y , z , 0), dielectric[tindg(x , y , z , 6)]/4.));
				trips.push_back(Trip(cur, vindg(x , y , zp, 0), dielectric[tindg(x , y , z , 6)]/4.));
				trips.push_back(Trip(cur, vindg(xm, y , zp, 0), dielectric[tindg(x , y , z , 6)]/4.));
				trips.push_back(Trip(cur, vindg(xm, y , z , 0), dielectric[tindg(x , y , z , 6)]/4.));

				trips.push_back(Trip(cur, vindg(x , y , z , 1), dielectric[tindg(x , y , z , 7)]/4.));
				trips.push_back(Trip(cur, vindg(x , y , zp, 1), dielectric[tindg(x , y , z , 7)]/4.));
				trips.push_back(Trip(cur, vindg(x , ym, zp, 1), dielectric[tindg(x , y , z , 7)]/4.));
				trips.push_back(Trip(cur, vindg(x , ym, z , 1), dielectric[tindg(x , y , z , 7)]/4.));

				trips.push_back(Trip(cur, vindg(x , y , z , 2), dielectric[tindg(x , y , z , 8)]   ));
			}
		}
	}
	nu_tensor_op.setFromTriplets(trips.begin(), trips.end());
}

//create a tensor divergence operator (Yee)
void Simulation::stencil_tensor_divergence() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*6*3);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
		for(int y = -2; y < Ny+2; y++) {
			for(int z = -2; z < Nz+2; z++) {
				//make it work in 1D, 2D as well
				if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
					continue;
				if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
					continue;
				if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
					continue;

				const int xm = (Nx > 1) ? x-1 : x;
				const int ym = (Ny > 1) ? y-1 : y;
				const int zm = (Nz > 1) ? z-1 : z;

				int cur = vindg(x, y, z, 0);

				trips.push_back(Trip(cur, tindg(x , y , z , 0),  1./dx));
				trips.push_back(Trip(cur, tindg(xm, y , z , 0), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 3),  1./dx));
				trips.push_back(Trip(cur, tindg(x , ym, z , 3), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 6),  1./dx));
				trips.push_back(Trip(cur, tindg(x , y , zm, 6), -1./dx));

				cur = vindg(x, y, z, 1);

				trips.push_back(Trip(cur, tindg(x , y , z , 1),  1./dx));
				trips.push_back(Trip(cur, tindg(xm, y , z , 1), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 4),  1./dx));
				trips.push_back(Trip(cur, tindg(x , ym, z , 4), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 7),  1./dx));
				trips.push_back(Trip(cur, tindg(x , y , zm, 7), -1./dx));

				cur = vindg(x, y, z, 2);

				trips.push_back(Trip(cur, tindg(x , y , z , 2),  1./dx));
				trips.push_back(Trip(cur, tindg(xm, y , z , 2), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 5),  1./dx));
				trips.push_back(Trip(cur, tindg(x , ym, z , 5), -1./dx));

				trips.push_back(Trip(cur, tindg(x , y , z , 8),  1./dx));
				trips.push_back(Trip(cur, tindg(x , y , zm, 8), -1./dx));
			}
		}
	}

	tensor_divergence.setFromTriplets(trips.begin(), trips.end());
}

//create a curl operator operating on the electric field (Yee)
void Simulation::stencil_curl_E() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*9);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
	for(int y = -2; y < Ny+2; y++) {
	for(int z = -2; z < Nz+2; z++) {
		//make it work in 1D, 2D as well
		if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
			continue;
		if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
			continue;
		if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
			continue;

		const int xp = (Nx > 1) ? x+1 : x;
		const int yp = (Ny > 1) ? y+1 : y;
		const int zp = (Nz > 1) ? z+1 : z;

		int cur = vindg(x, y, z, 0);
		trips.push_back(Trip(cur, vindg(x , yp, z , 2),  1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 2), -1./dx));
		trips.push_back(Trip(cur, vindg(x , y , zp, 1), -1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 1),  1./dx));

		cur = vindg(x, y, z, 1);
		trips.push_back(Trip(cur, vindg(x , y , zp, 0),  1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 0), -1./dx));
		trips.push_back(Trip(cur, vindg(xp, y , z , 2), -1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 2),  1./dx));

		cur = vindg(x, y, z, 2);
		trips.push_back(Trip(cur, vindg(xp, y , z , 1),  1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 1), -1./dx));
		trips.push_back(Trip(cur, vindg(x , yp, z , 0), -1./dx));
		trips.push_back(Trip(cur, vindg(x , y , z , 0),  1./dx));
	}}}
	
	curl_E.setFromTriplets(trips.begin(), trips.end());
}

//create a curl operator operating on the magnetic field (Yee)
void Simulation::stencil_curl_B() {
	vector<Trip> trips;
	trips.reserve(total_size.prod()*12);

	//operators should overlap at borders
	//all cells + a few border cells
	for(int x = -2; x < Nx+2; x++) {
		for(int y = -2; y < Ny+2; y++) {
			for(int z = -2; z < Nz+2; z++) {
				//make it work in 1D, 2D as well
				if(x >= Nx+ghost_cells[0] || x < -ghost_cells[0])
					continue;
				if(y >= Ny+ghost_cells[1] || y < -ghost_cells[1])
					continue;
				if(z >= Nz+ghost_cells[2] || z < -ghost_cells[2])
					continue;

				const int xm = (Nx > 1) ? x-1 : x;
				const int ym = (Ny > 1) ? y-1 : y;
				const int zm = (Nz > 1) ? z-1 : z;

				int cur = vindg(x, y, z, 0);
				trips.push_back(Trip(cur, vindg(x , y , z , 2),  1./dx));
				trips.push_back(Trip(cur, vindg(x , ym, z , 2), -1./dx));
				trips.push_back(Trip(cur, vindg(x , y , z , 1), -1./dx));
				trips.push_back(Trip(cur, vindg(x , y , zm, 1),  1./dx));

				cur = vindg(x, y, z, 1);
				trips.push_back(Trip(cur, vindg(x , y , z , 0),  1./dx));
				trips.push_back(Trip(cur, vindg(x , y , zm, 0), -1./dx));
				trips.push_back(Trip(cur, vindg(x , y , z , 2), -1./dx));
				trips.push_back(Trip(cur, vindg(xm, y , z , 2),  1./dx));

				cur = vindg(x, y, z, 2);
				trips.push_back(Trip(cur, vindg(x , y , z , 1),  1./dx));
				trips.push_back(Trip(cur, vindg(xm, y , z , 1), -1./dx));
				trips.push_back(Trip(cur, vindg(x , y , z , 0), -1./dx));
				trips.push_back(Trip(cur, vindg(x , ym, z , 0),  1./dx));
			}
		}
	}

	curl_B.setFromTriplets(trips.begin(), trips.end());
}
