// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#ifndef CONSTANTS_H
#define CONSTANTS_H

//Physical constants
namespace SI
{
	static const double e = 1.60217646e-19;
	static const double c = 299792458;
	static const double me = 9.10938188e-31;
	static const double kb = 1.3806503e-23;
	static const double e_me = 1.75882017e11;
	static const double mp_me = 1836.15266;
	static const double eps0 = 8.854187817620e-12;
	static const double mu0 = 1.2566370614e-6;
}

#endif
