#include "simulation.h"

void
Simulation::boundaries_processing()
{
	// Left bound
	if (comm.get_cell_offset(0) == 0)
	{
		for(int y = -ghost_cells[0]; y < Ny + ghost_cells[1]; ++y) {
		for(int gx = 0; gx <= ghost_cells[0]; ++gx)
		{
			E[vindg(0 - gx, y, 0, 0)] = E[vindg(1, y, 0, 0)];
			E[vindg(0 - gx, y, 0, 1)] = 0.;
			E[vindg(0 - gx, y, 0, 2)] = 0.;
		}}
	}
	
	// Right bound
	if (Nx + comm.get_cell_offset(0) == comm.get_global_cell_size(0))
	{
		for(int y = -ghost_cells[0]; y < Ny + ghost_cells[1]; ++y) {
		for(int gx = 0; gx < ghost_cells[0]; ++gx)
		{
			E[vindg(Nx + gx, y, 0, 0)] = E[vindg(Nx-1, y, 0, 0)];
			E[vindg(Nx + gx, y, 0, 1)] = 0.;
			E[vindg(Nx + gx, y, 0, 2)] = 0.;
		}}
	}

	// Lower bound
	if (comm.get_cell_offset(1) == 0)
	{
		for(int x = -ghost_cells[0]; x < Nx + ghost_cells[0]; ++x) {
		for(int gy = 0; gy <= ghost_cells[1]; ++gy)
		{
			E[vindg(x, 0 - gy, 0, 0)] = 0.;
			E[vindg(x, 0 - gy, 0, 1)] = E[vindg(x, 1, 0, 1)]; 
			E[vindg(x, 0 - gy, 0, 2)] = 0.;
		}}
	}

	// Upper bound
	if (Ny + comm.get_cell_offset(1) == comm.get_cell_offset(1))
	{
		for(int x = -ghost_cells[0]; x < Nx + ghost_cells[0]; ++x) {
		for(int gy = 0; gy < ghost_cells[1]; ++gy)
		{
			E[vindg(x, Ny + gy, 0, 0)] = 0.;
			E[vindg(x, Ny + gy, 0, 1)] = E[vindg(x, Ny-1, 0, 1)];
			E[vindg(x, Ny + gy, 0, 2)] = 0.;
		}}
	}
}