// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

//function to smooth a scalar field
void Simulation::smooth(Field& rho, int dim) {
	Field tmp(Field::Constant(rho.size(), std::numeric_limits<double>::quiet_NaN()));

	//binomial filter
	for(int x = 0; x < Nx; x++) {
		for(int y = 0; y < Ny; y++) {
			for(int z = 0; z < Nz; z++) {
				for(int c = 0; c < dim; c++) {
					const int xp = (Nx > 1) ? x+1 : x;
					const int xm = (Nx > 1) ? x-1 : x;
					const int yp = (Ny > 1) ? y+1 : y;
					const int ym = (Ny > 1) ? y-1 : y;
					const int zp = (Nz > 1) ? z+1 : z;
					const int zm = (Nz > 1) ? z-1 : z;
					tmp[dindg(x, y, z, c, dim)] =
						((rho[dindg(xm,ym,zm,c,dim)] * 0.25 + rho[dindg(xm,ym,z ,c,dim)] * 0.5 + rho[dindg(xm,ym,zp,c,dim)] * 0.25)*0.25)*0.25+
						((rho[dindg(xm,y ,zm,c,dim)] * 0.25 + rho[dindg(xm,y ,z ,c,dim)] * 0.5 + rho[dindg(xm,y ,zp,c,dim)] * 0.25)*0.50)*0.25+
						((rho[dindg(xm,yp,zm,c,dim)] * 0.25 + rho[dindg(xm,yp,z ,c,dim)] * 0.5 + rho[dindg(xm,yp,zp,c,dim)] * 0.25)*0.25)*0.25+
						((rho[dindg(x ,ym,zm,c,dim)] * 0.25 + rho[dindg(x ,ym,z ,c,dim)] * 0.5 + rho[dindg(x ,ym,zp,c,dim)] * 0.25)*0.25)*0.50+
						((rho[dindg(x ,y ,zm,c,dim)] * 0.25 + rho[dindg(x ,y ,z ,c,dim)] * 0.5 + rho[dindg(x ,y ,zp,c,dim)] * 0.25)*0.50)*0.50+
						((rho[dindg(x ,yp,zm,c,dim)] * 0.25 + rho[dindg(x ,yp,z ,c,dim)] * 0.5 + rho[dindg(x ,yp,zp,c,dim)] * 0.25)*0.25)*0.50+
						((rho[dindg(xp,ym,zm,c,dim)] * 0.25 + rho[dindg(xp,ym,z ,c,dim)] * 0.5 + rho[dindg(xp,ym,zp,c,dim)] * 0.25)*0.25)*0.25+
						((rho[dindg(xp,y ,zm,c,dim)] * 0.25 + rho[dindg(xp,y ,z ,c,dim)] * 0.5 + rho[dindg(xp,y ,zp,c,dim)] * 0.25)*0.50)*0.25+
						((rho[dindg(xp,yp,zm,c,dim)] * 0.25 + rho[dindg(xp,yp,z ,c,dim)] * 0.5 + rho[dindg(xp,yp,zp,c,dim)] * 0.25)*0.25)*0.25;
				}
			}
		}
	}
	comm.move_fields(tmp, dim);

	//compensation pass
	for(int x = 0; x < Nx; x++) {
		for(int y = 0; y < Ny; y++) {
			for(int z = 0; z < Nz; z++) {
				for(int c = 0; c < dim; c++) {
					const int xp = (Nx > 1) ? x+1 : x;
					const int xm = (Nx > 1) ? x-1 : x;
					const int yp = (Ny > 1) ? y+1 : y;
					const int ym = (Ny > 1) ? y-1 : y;
					const int zp = (Nz > 1) ? z+1 : z;
					const int zm = (Nz > 1) ? z-1 : z;
					rho[dindg(x, y, z, c, dim)] =
						((tmp[dindg(xm,ym,zm,c,dim)] * -0.25 + tmp[dindg(xm,ym,z ,c,dim)] * 1.5 - tmp[dindg(xm,ym,zp,c,dim)] * 0.25)*-0.25)*-0.25+
						((tmp[dindg(xm,y ,zm,c,dim)] * -0.25 + tmp[dindg(xm,y ,z ,c,dim)] * 1.5 - tmp[dindg(xm,y ,zp,c,dim)] * 0.25)* 1.50)*-0.25+
						((tmp[dindg(xm,yp,zm,c,dim)] * -0.25 + tmp[dindg(xm,yp,z ,c,dim)] * 1.5 - tmp[dindg(xm,yp,zp,c,dim)] * 0.25)*-0.25)*-0.25+
						((tmp[dindg(x ,ym,zm,c,dim)] * -0.25 + tmp[dindg(x ,ym,z ,c,dim)] * 1.5 - tmp[dindg(x ,ym,zp,c,dim)] * 0.25)*-0.25)* 1.50+
						((tmp[dindg(x ,y ,zm,c,dim)] * -0.25 + tmp[dindg(x ,y ,z ,c,dim)] * 1.5 - tmp[dindg(x ,y ,zp,c,dim)] * 0.25)* 1.50)* 1.50+
						((tmp[dindg(x ,yp,zm,c,dim)] * -0.25 + tmp[dindg(x ,yp,z ,c,dim)] * 1.5 - tmp[dindg(x ,yp,zp,c,dim)] * 0.25)*-0.25)* 1.50+
						((tmp[dindg(xp,ym,zm,c,dim)] * -0.25 + tmp[dindg(xp,ym,z ,c,dim)] * 1.5 - tmp[dindg(xp,ym,zp,c,dim)] * 0.25)*-0.25)*-0.25+
						((tmp[dindg(xp,y ,zm,c,dim)] * -0.25 + tmp[dindg(xp,y ,z ,c,dim)] * 1.5 - tmp[dindg(xp,y ,zp,c,dim)] * 0.25)* 1.50)*-0.25+
						((tmp[dindg(xp,yp,zm,c,dim)] * -0.25 + tmp[dindg(xp,yp,z ,c,dim)] * 1.5 - tmp[dindg(xp,yp,zp,c,dim)] * 0.25)*-0.25)*-0.25;
				}
			}
		}
	}

	comm.move_fields(rho, dim);
}
