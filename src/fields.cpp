// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <algorithm>
#include <iostream>

#include <unsupported/Eigen/IterativeSolvers>

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

typedef Eigen::GMRES<Eigen::SparseMatrix<double, Eigen::ColMajor> > Solver;

//calculate time-advanced fields
void Simulation::calc_fields() {
	//stow away previous fields for later use
	E_old = E;
	B_old = B;

	//calculate \hat{\rho} and distribute among neighbors
	Field charge_density_hat = charge_density - THETA*dt * divergence * (current_density - dt * div_P);
	comm.move_fields(charge_density_hat, 1);

	//get the left-hand side of the E_n+\Theta electric field equation
	//0.5 cancels with 0.5 of rhs (GMRES won't solve identity*E=E, 0.5*identity*E=0.5*E is fine
	const Operator oper = 0.5*(
	  (SI::c*SI::c*dt*dt*THETA*THETA)
	  * (
	    - vector_laplace
	    + vector_laplace * nu_tensor_op
	  )
	  + (identity - nu_tensor_op)
	);

	//get the right-hand side of the E_n+\Theta electric field equation and distribute
	//0.5 cancels with 0.5 of oper (GMRES won't solve identity*E=E, 0.5*identity*E=0.5*E is fine
	Field rhs = 0.5*(
	    E_old
	    - (SI::c*SI::c * THETA * dt)
	      * (
	        + SI::mu0 * current_density
	        - SI::mu0 * dt/2. * div_P
	        - curl_B * B
	      )
	    - (SI::c*SI::c * dt*dt * THETA*THETA / SI::eps0)
	      * gradient * charge_density_hat
	);

	comm.move_fields(rhs, 3);

	//prepare GMRES solver
	Solver gmres(oper);
	gmres.setTolerance(GMRES_TOLERANCE);
	gmres.setMaxIterations(6);
	gmres.set_restart(6);

	//Solve equation, distribute border values, solve again...
	//Here, a fixed number of iterations is used
	int i = 0;
	do {
		E = gmres.solveWithGuess(rhs, E);
		comm.move_fields(E, 3);
		i++;
	} while(gmres.info() == Eigen::Success && i < SCHWARZ_ITERS);

	if(gmres.info() != Eigen::Success) {
		cout << "Field GMRES failed!" << endl;
		cout << i << endl;
		cout << gmres.error() << endl;
	}

	//update magnetic field with new electric field and distribute
	B -= dt*curl_E*E;

	comm.move_fields(B, 3);
}

//interpolate field values to particle position, basically inversion of the procedure distributing the source terms
void Simulation::get_local_fields(const Field& electric, const Field& magnetic, const Vector3d& pos, Vector3d& E_loc, Vector3d& B_loc, uint8_t flav) const {
	static double formx0 [5];
	static double formx05[5];
	static double formy0 [5];
	static double formy05[5];
	static double formz0 [5];
	static double formz05[5];

	E_loc = {0., 0., 0.};
	B_loc = {0., 0., 0.};

	if (flav == FLAVOR_PROTON_JET)
	{
		B_loc = {0., 0., parameters.at("B0_z")};
		return;
	}
	
	//particle positions are in units of dx
	const int l = static_cast<int>(floor(pos[0]))-2;
	const int m = static_cast<int>(floor(pos[1]))-2;
	const int n = static_cast<int>(floor(pos[2]))-2;

	//calculate form factors for particle
	for(int aa = 0; aa < 5; aa++) {
		formx0 [aa] = (Nx > 1) ? weight(pos[0], l+aa)      : 1;
		formx05[aa] = (Nx > 1) ? weight(pos[0], l+aa, 0.5) : 1;
		formy0 [aa] = (Ny > 1) ? weight(pos[1], m+aa)      : 1;
		formy05[aa] = (Ny > 1) ? weight(pos[1], m+aa, 0.5) : 1;
		formz0 [aa] = (Nz > 1) ? weight(pos[2], n+aa)      : 1;
		formz05[aa] = (Nz > 1) ? weight(pos[2], n+aa, 0.5) : 1;
	}

	//for all cells touched by TSC particle
	for(int aa = 0; aa < min(5,Nx); aa++) {
		for(int bb = 0; bb < min(5,Ny); bb++) {
			for(int cc = 0; cc < min(5,Nz); cc++) {
				//interpolate electric and magnetic field for Yee scheme
				E_loc[0] += formx05[aa] * formy0 [bb] * formz0 [cc] * electric[vindg(l+aa, m+bb, n+cc, 0)];
				E_loc[1] += formx0 [aa] * formy05[bb] * formz0 [cc] * electric[vindg(l+aa, m+bb, n+cc, 1)];
				E_loc[2] += formx0 [aa] * formy0 [bb] * formz05[cc] * electric[vindg(l+aa, m+bb, n+cc, 2)];

				B_loc[0] += formx0 [aa] * formy05[bb] * formz05[cc] * magnetic[vindg(l+aa, m+bb, n+cc, 0)];
				B_loc[1] += formx05[aa] * formy0 [bb] * formz05[cc] * magnetic[vindg(l+aa, m+bb, n+cc, 1)];
				B_loc[2] += formx05[aa] * formy05[bb] * formz0 [cc] * magnetic[vindg(l+aa, m+bb, n+cc, 2)];
			}
		}
	}
}

