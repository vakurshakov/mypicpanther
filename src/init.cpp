// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#include <iostream>
#include <random>

#include "simulation.h"

using namespace std;

using Eigen::Vector3d;

//init general physical values for the simulation
void Simulation::init_parameters() {
	double rescale_dx  = parameters.at("rescale_dx");
	double rescale_dt  = parameters.at("rescale_dt");

	double plasma_freq = parameters.at("plasma_freq");

	mp_me             = parameters.at("mp_over_me");
	temperature       = parameters.at("temperature");

	electronnumberbg  = parameters.at("num_e_bg");
	protonnumberbg    = parameters.at("num_P_bg");
	positronnumberbg  = parameters.at("num_p_bg");
	electronnumberjet = parameters.at("num_e_jet");
	protonnumberjet   = parameters.at("num_P_jet");
	positronnumberjet = parameters.at("num_p_jet");

	double protonJetDensity = parameters.at("proton_jet_density");

	//electron density, thermal velocity, temperature, debye length
	double e_density = plasma_freq * plasma_freq * SI::me * SI::eps0 / (SI::e * SI::e);
	double temp_K = temperature / (SI::kb / SI::e); // k [eV / K] * T [K] ~ [eV], 
	double debye = sqrt(SI::eps0 * SI::kb * temp_K / (e_density * SI::e * SI::e)); // 1 eV = SI::e J 

	//get a suitable dx from debye length
	dx = sqrt(0.5) * debye / rescale_dx;
	cell_vol = dx * dx * dx;

	//calculate macro factors for all particle types
	double p_density = e_density;

	//calculate timesteps needed to fulfill langmuir, CFL criteria
	double dt_langmuir = 1.0 / (2. * plasma_freq);
	double dt_cfl      = dx / (sqrt(3.0) * SI::c);
	dt = dt_cfl / rescale_dt;

	//initial magnetic field values and a few misc quantities
	Vector3d B0(parameters.at("B0_x"), parameters.at("B0_y"), parameters.at("B0_z"));
	double mean_B = B0.norm();
	double v_alfven = mean_B / sqrt(SI::mu0 * p_density * mp_me * SI::me);

	double soundspeed = sqrt(5. / 3. * (temperature * SI::e) / SI::me / mp_me);

	//prepare particle data for each particle type
	if(protonnumberbg + electronnumberbg > 0) {
		Particle::p[FLAVOR_ELECTRON] = {SI::me      , -SI::e, -SI::e/(2.*SI::me      ), e_density / (double)electronnumberbg};
		Particle::p[FLAVOR_PROTON  ] = {SI::me*mp_me,  SI::e,  SI::e/(2.*SI::me*mp_me), p_density / (double)protonnumberbg};
	}
	else {
		macro = macro_e = macro_P = 1;
	}
	
	if (protonnumberjet + electronnumberjet > 0)
	{
		Particle::p[FLAVOR_ELECTRON_JET] = {SI::me	  	, -SI::e, -SI::e/(2. * SI::me), protonJetDensity / (double)electronnumberjet};
		Particle::p[FLAVOR_PROTON_JET]	 = {SI::me*mp_me, +SI::e, +SI::e/(2. * SI::me * mp_me), protonJetDensity / (double)protonnumberjet};
	}
	
	//set dx and dt for particles
	Particle::set_dt(dt);
	Particle::set_dx(dx);

	if(comm.rank() == 0 && debug) {
		cout << "Plasma freq. e: " << plasma_freq << endl;
		cout << "Density: " << p_density+e_density << endl;
		cout << "Temperature: " << temperature << " eV" << endl;
		cout << "Debye length: " << debye << endl;
		cout << "dx: " << dx << endl;
		cout << "Langmuir step: " << dt_langmuir << ", CFL step: " << dt_cfl << endl;
		cout << "dt: " << dt << endl;
		cout << "macrofactor = macrofactor e: " << macro_e << endl;
		cout << "macrofactor P: " << macro_P << endl;
		cout << "Alfven speed: " << v_alfven << endl;
		cout << "Sound speed: " << soundspeed << endl;
	}
}

//unused function to prepare source terms, they are overwritten anyway
void Simulation::init_sources()
{
	return;
}

//set initial background fields, only magnetic field here
void Simulation::init_fields()
{
	Vector3d B0(parameters.at("B0_x"), parameters.at("B0_y"), parameters.at("B0_z"));

	for(int x = -ghost_cells[0]; x < Nx + ghost_cells[0]; x++) {
	for(int y = -ghost_cells[1]; y < Ny + ghost_cells[1]; y++) {
	for(int z = 0; z < Nz; z++) {
		B[vindg(x, y, z, 0)] = B0[0];
		B[vindg(x, y, z, 1)] = B0[1];
		B[vindg(x, y, z, 2)] = B0[2];
	}}}

	comm.move_fields(B, 3);
	B_old = B;
}


//create particle populations
void Simulation::init_particles() {
	//random number generator
	mt19937_64 eng(parameters.at("seed") + comm.rank());

	int ppc = Nx * Ny * Nz * (
			electronnumberbg +
			protonnumberbg
		);

	parts.reserve(ppc);

	{
		double sigma_u = sqrt(temperature / 511.0e3) ; // u = gamma * v

		uniform_real_distribution<double> uniform(0., 1.);
		normal_distribution<double> normal(0, sigma_u);

		Tag id;

		id.guid = 0;
		//put particles into each cell
		for(int x = 0; x < Nx; x++) {
		for(int y = 0; y < Ny; y++) {
		for(int z = 0; z < Nz; z++) {
		if (cell_in_a_circle(comm.get_cell_offset(0) + x, comm.get_cell_offset(1) + y, z)) {
			// ID by global cell index, population ID, flavor ID and local counter			
			id.start_cell_index = ind(x, y, z, 0, Nx, Ny, Nz, 1);

			id.population = POP_BG;
			id.id_in_cell = 0;

			// create bg electrons
			id.flav = FLAVOR_ELECTRON;
			for(int i = 0; i < electronnumberbg; i++) {
				// gaussian velocity components
				Vector3d beta(normal(eng), normal(eng), normal(eng));
				beta /= sqrt(1. + beta.dot(beta));
				// drift
				// beta = boost_beta(beta, boost_b);

				// random position within the cell, 1D, 2D, 3D in units dx
				Vector3d pos(static_cast<double>(x), static_cast<double>(y), static_cast<double>(z));
				
				pos[0] += (Nx > 1) ? uniform(eng) : 0.5;
				pos[1] += (Ny > 1) ? uniform(eng) : 0.5;
				pos[2] += (Nz > 1) ? uniform(eng) : 0.5;	

				// put into list
				parts.push_back(Particle(pos, beta, id));
				id.id_in_cell++;
			}// repeat for protons, positrons and for jet

			//create bg protons
			id.flav = FLAVOR_PROTON;
			for(int i = 0; i < protonnumberbg; i++) {
				//gaussian velocity components
				Vector3d beta(normal(eng), normal(eng), normal(eng));
				beta /= sqrt(1. + beta.dot(beta));
				beta /= sqrt(mp_me); //protons are heavier -> slower in thermal equilibrium
				//drift
				//beta = boost_beta(beta, boost_b);

				//random position within the cell, 1D, 2D, 3D in units dx
				Vector3d pos(static_cast<double>(x), static_cast<double>(y), static_cast<double>(z));
				pos[2] += (Nz > 1) ? uniform(eng) : 0.5;
				pos[1] += (Ny > 1) ? uniform(eng) : 0.5;
				pos[0] += (Nx > 1) ? uniform(eng) : 0.5;

				//put into list
				parts.push_back(Particle(pos, beta, id));
				id.id_in_cell++;
			}
		}}}}
	}
	parts.shrink_to_fit();
}

bool
Simulation::cell_in_a_circle(int x_global, int y_global, int z_global)
{
	if (z_global > 0) throw "Error: bool Simulation::cell_in_a_circle(int, int, int) is two-dimensional (x,y)";

	static const int x_center = comm.get_global_cell_size(0) / 2;
	static const int y_center = comm.get_global_cell_size(1) / 2;
	static const int plasma_radius = std::round(parameters.at("plasma_radius") / dx);

	return (
		(x_global - x_center) * (x_global - x_center) + 
		(y_global - y_center) * (y_global - y_center) <=
		plasma_radius * plasma_radius
	);
}