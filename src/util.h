// Author: Andreas Kempf, Ruhr-Universitaet Bochum, ank@tp4.rub.de
// (c) 2014, for licensing details see the LICENSE file

#pragma once

#ifndef UTIL_H
#define UTIL_H

#include <Eigen/Sparse>
#include <Eigen/Dense>

typedef Eigen::SparseMatrix<double, Eigen::ColMajor> Operator;
typedef Eigen::VectorXd Field;

//general indexing routine (row major)
inline constexpr int ind(int x, int y, int z, int c, int Nx, int Ny, int Nz, int Nc) {
	return(c + Nc*(z + Nz*(y + Ny*x)));
}

//Lorentz boost a velocity
inline Eigen::Vector3d boost_beta(const Eigen::Vector3d& beta, const Eigen::Vector3d& betap) {
	const double beta_dot_betap = beta.dot(betap);
	const double betap_sq = betap.dot(betap);

	if(betap_sq == 0.) return beta;

	const Eigen::Vector3d beta_parallel = beta_dot_betap / betap_sq * betap;
	const Eigen::Vector3d beta_perp = beta - beta_parallel;
	return(betap + beta_parallel + sqrt(1. - betap_sq) * beta_perp) / (1. + beta_dot_betap);
};

//calculate weight for particle position and grid point, Triangular Shaped Cloud (second order)
inline double weight(double partpos, int index, double grid_shift = 0.) {
	double distance = fabs(partpos - index - grid_shift);

	if (distance <= 0.5)
		return 0.75 - (distance*distance);
	distance -= 1.5;
	if (distance <= 0.)
		return 0.5 * distance*distance;
	return 0.;
}

//fails if one component is NaN
inline bool valid_velocity(const Eigen::Vector3d& u) {
	return(u.dot(u) >= 0.);
}

#endif
