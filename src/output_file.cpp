#include "output_file.h"

#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>

#include "util.h"

namespace fs = std::filesystem;


//set up HDF5 output
Binary_output_files::Binary_output_files(Communicator& comm, const double& dx, const double& dt, const double& time, const int& timestep)
    :   Output_file(comm, dx, dt, time, timestep)
    {
	    //create file on rank 0
	    if(comm.rank() == 0) {
            fs::create_directory(fs::path("./out"));
        }
	    comm.barrier();
    }


void
Output_file::create_skeleton()
{
    for(const auto& [name, _1, _2, component]: list_of_components)
    {
        std::string path = "./out/fields/" + name +
			"/" + name + std::to_string(component) +
			"/cpu_" + std::to_string(comm.rank());
		
        fs::create_directories(fs::path(path));
	
		std::ofstream diagnostic_parameters_(path + "/parameters.txt", std::ios::out);
		diagnostic_parameters_ << "#dx, dt" << std::endl;
		diagnostic_parameters_ << dx << " " << dt << std::endl;
		diagnostic_parameters_ << "#sizeof(float)" << std::endl;
		diagnostic_parameters_ << sizeof(float) << std::endl;
		diagnostic_parameters_ << "#offset_x, offset_y" << std::endl;
		diagnostic_parameters_ << comm.get_cell_offset(0) << " " << comm.get_cell_offset(1) << std::endl;
	}

	//write particle data, if due
	if (const auto& [diagnostic_timestep, particles] = output_particles;
        diagnostic_timestep > 0)
        {
			std::string path = "./out/particles/cpu_" + std::to_string(comm.rank());
            fs::create_directories(fs::path(path));

			std::ofstream diagnostic_parameters_("./out/particles/parameters.txt", std::ios::out);
	        diagnostic_parameters_ << "#dx, dt" << std::endl;
	        diagnostic_parameters_ << dx << " " << dt << std::endl;
	        diagnostic_parameters_ << "#sizeof(Particle) = 2 * sizeof(double) + 2 * sizeof(uint_8)" << std::endl;
	        diagnostic_parameters_ << sizeof(double) << " " << sizeof(uint8_t) <<  std::endl;
		}
}


void
Binary_output_files::output()
{
    if(timestep == 0)
		create_skeleton();
	comm.barrier();

	//write all vector fields in our list, if due
	for(const auto& [name, diagnostic_timestep, field, component]: list_of_components) {
		if(timestep % diagnostic_timestep == 0)
        {	
            std::string path = "./out/fields/" + name +
				"/" + name + std::to_string(component) +
				"/cpu_" + std::to_string(comm.rank()) +
				"/" + std::to_string(timestep);
			
			write_vector_field(path, *field, component);
		}
	}

	//write particle data, if due
	if(const auto& [diagnostic_timestep, particles] = output_particles;
        diagnostic_timestep && timestep % diagnostic_timestep == 0)
        {
	    	particle_output(*particles);
	    }

	comm.barrier();
}


void
Binary_output_files::write_vector_field(const std::string& path, const Field& field, int component)
{
    std::ofstream bin_file(path + ".bin", std::ios::out | std::ios::binary);

	// in this approach we have to take every %component%-th number from the field
	for (int x = 0; x < comm.get_total_size(0); ++x) {
	for (int y = 0; y < comm.get_total_size(1); ++y) {
	for (int z = 0; z < comm.get_total_size(2); ++z)
	{
		const char* data = reinterpret_cast<const char*>(
			&field[ind(
				x, y, z, component,
				comm.get_total_size(0), comm.get_total_size(1), comm.get_total_size(2), 3)]	
		);

		bin_file.write(data, sizeof(double));
	}}}
}


void
Binary_output_files::particle_output(const std::vector<Particle>& particles)
{
	std::string path = "./out/particles/cpu_" + std::to_string(comm.rank());
	std::ofstream bin_file(path + "/" + std::to_string(timestep) + ".bin",
		std::ios::out | std::ios::binary);

	
	for (const auto& particle : particles)
	{
		const Eigen::Vector3d& r = particle.get_r();
		const Eigen::Vector3d& p = particle.get_m() * particle.get_u();
		const uint8_t flavour    = particle.get_f(); 
		const uint8_t population = particle.get_population();
	

		bin_file.write(reinterpret_cast<const char*>(r.data()), 3 * sizeof(double));
		bin_file.write(reinterpret_cast<const char*>(p.data()), 3 * sizeof(double));
		bin_file.write(reinterpret_cast<const char*>(&flavour), 	sizeof(uint8_t));
		bin_file.write(reinterpret_cast<const char*>(&population), 	sizeof(uint8_t));
	}
}