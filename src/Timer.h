#ifndef TIMER_H_
#define TIMER_H_
#include "mpi.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <map>
#include <vector>

struct Timer{
    FILE *fTimes;
    std::map<std::string,double> times;
    std::map<std::string,double> startT;
    bool firstWrite;
    Timer(const std::string& filename){
        fTimes = fopen( (".//"+filename).c_str(), "w");
        _reset = true;
        firstWrite = true;
    }

    void start(const std::string &s){
        startT[s] = MPI_Wtime();
    }
    void finish(const std::string &s){

        times[s] += MPI_Wtime() - startT[s];
    }
    void reset(){
        for (auto it = times.begin(); it != times.end(); ++it){
            it->second = 0.;
        }
        _reset = true;
    }
    void write(long timestep, int delay); 
protected:
    bool _reset;
};

#endif 	
