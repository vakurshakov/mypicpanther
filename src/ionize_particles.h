#ifndef COMMAND_IONIZE_PARTICLES_HPP
#define COMMAND_IONIZE_PARTICLES_HPP

//#######################################################################################

#include <vector>
#include <functional>

#include "particle.h"
#include "communicator.h"

size_t
get_amount(size_t protonJetNumber, double r_larm, double dr, double dx);

/**
 * @brief Command that sets fixed number of particles (in pairs)
 * 		in the computational domain each time step.
 * 
 * @todo Ionziation process with more than one buffer particle.
 */
class Ionize_particles {
public:
	/**
	 * Constructor of the command.
	 * 
	 * @param comm  Communicator that will give a full description about simulation
	 * @param list_of_particle List of every particles in the simulation
	 * @param ions_flav	Flavour of particles that will be ions.
	 * @param vi_0		Initial velocity of an ions.
	 * @param lost_flav	Flavour of buffer particles to conserve charge.
	 * @param steps_of_injection Duration of injection in timesteps.
	 * @param r_larm    Radius where particles will be ionized.
	 * @param dr        Width of that ring.
	 * @param dx        Grid width.
	 * 
	 * @todo Set the command in the builder of particles
	 */
	Ionize_particles(
		Communicator& comm,
		std::vector<Particle>& list_of_particles,
		uint8_t ions_flav, double vi_0, double Ti_x, double Ti_y, double Ti_z,
		uint8_t lost_flav, double Tl_x, double Tl_y, double Tl_z,
		size_t steps_of_injection, size_t protonJetNumber,
		double r_larm, double dr, const double& dx)
			:	comm(comm),
				list_of_particles(list_of_particles),
				ions_flav(ions_flav), ui_0(vi_0 / sqrt(1. - vi_0 * vi_0)), Ti({Ti_x, Ti_y, Ti_z}),
				lost_flav(lost_flav), ul_0(vi_0 / sqrt(1. - vi_0 * vi_0)), Tl({Tl_x, Tl_y, Tl_z}),
				steps_of_injection(steps_of_injection), protonJetNumber(protonJetNumber),
				r_larm(r_larm), dr(dr), dx(dx)
				{};

	/**
	 * @brief On a concrete time step t, it loads the number of particles
	 * 		(according to amount_of_particles_to_load_per_step) in pairs into
	 * 		a computatoinal domain with some space and momentum distributions. 
	 * 
	 * @param t Outer time step to load a concrete
	 * 		amount of particles on that step.get_amount
	 */
	void execute(size_t t);

	/**
	 * @brief Checks whether command needs to be removed
	 * 		from the simulation or not.
	 * 
	 * @param t Outer time step, base of desicion.
	 * 
	 * @return True if the time is greater or equal to
	 * 		the time of injection, false otherwise. 
	 */
	bool should_be_removed(size_t t) const {
		return !(1260-1 <= t && t < steps_of_injection);
	}

private:
	bool
	part_of_area_in_simulation_domain();

	bool
	picked_point_in_simulation_domain(const Eigen::Vector3d& r);

	Eigen::Vector3d
	pick_point_on_annulus();

	Eigen::Vector3d
	pick_annular_impulse(
		const Eigen::Vector3d& r, double p0,
		double mass, const Eigen::Vector3d& temperature);

	double
	temperature_impulse(double temperature, double mass);

	double
	uniform_probability(const Eigen::Vector3d& r);

	size_t
	amount_of_particles_to_load(size_t timestep);

private:
	Communicator& comm;
	
	std::vector<Particle>& list_of_particles;

	uint8_t ions_flav;
	const double ui_0;
	const Eigen::Vector3d Ti;
	
	uint8_t lost_flav;
	const double ul_0;
	const Eigen::Vector3d Tl;

	size_t steps_of_injection;
	size_t protonJetNumber;

	const double  r_larm;
	const double  dr;
	const double& dx;
};

//#######################################################################################

#endif // COMMAND_IONIZE_PARTICLES_HPP