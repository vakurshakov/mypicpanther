import os
import numpy as np
import struct
import collections
import pprint
from math import pi,sqrt
#усреднение бегущим окном
def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
#прочесть поглощённое вдоль линии
def ReadDamps(Path):
	files=os.listdir(Path)#получили список файлов каталоге
	files=sorted(files)
	#col=np.linspace(0,6,6)
	#print(files)
	Result=[]
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	for i in range(0,len(files),1):
		data={}
		tempData=[]
		#data['Ex'],data['Ey'],data['Ez'],data['Bx'],data['By'],data['Bz'], = np.loadtxt(Path+'/'+files[i],skiprows=2,unpack=True,usecols=(0,1,2,3,4,5))
		data['Ex'],data['Ey'],data['Ez'],data['Bx'],data['By'],data['Bz'], = np.loadtxt(Path+'/'+files[i],skiprows=2,unpack=True,usecols=(0,1,2,3,4,5))
		for j in FieldsTitles:
			tempData.append(data[j])
		if(i>0):
			Result.append(np.asarray(tempData-Result[-1]))
		else:
			Result.append(np.asarray(tempData))

	Result=np.asarray(Result)
	return Result

#читает сохранённые на каждом большом шаге диагностик значения поглощённого в слоях
def ReadAllTextDamp(File,SystemParameters):
	Path='../Fields/1D/'+File+'/'
	files=sorted(os.listdir(Path))#получили список файлов в корневом каталоге
	DampFields={}
	temp={}
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	for field in FieldsTitles:
		DampFields[field]=[]
	for DampFile in files:
		temp['Ex'],temp['Ey'],temp['Ez'],temp['Bx'],temp['By'],temp['Bz'],=np.loadtxt(Path+DampFile, skiprows=2, unpack=True,usecols=[0,1,2,3,4,5])
		for field in FieldsTitles:
			DampFields[field].append(temp[field])
		

	return DampFields
#поглощённое в зависимости от времени, просуммированное по  DampRange Type=Power или Energy
def DampTime(FieldsRaw,DampRange,Type):
	Fields={}
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	MaxDampStep=len(FieldsRaw['Ex'])
	for field in FieldsTitles:
		Fields[field]=[]
		Fields[field].append(FieldsRaw[field][0])
		Fields[field][0]=0
		for t in range(1,MaxDampStep):
			if Type=='Power':
				Fields[field].append(FieldsRaw[field][t]-FieldsRaw[field][t-1])
			if Type=='Energy':
				Fields[field].append(FieldsRaw[field][t])
			Fields[field][t]=np.sum(Fields[field][t][DampRange[0]:DampRange[1]], axis=0)
	return Fields
	
def EdgeDampTime(FieldsRaw,DampRange,Type):#на торцах
	Fields={}
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	MaxDampStep=len(FieldsRaw['Ex'])
	for field in FieldsTitles:
		Fields[field]=[]
		Fields[field].append(FieldsRaw[field][0])
		Fields[field][0]=0
		for t in range(1,MaxDampStep):
			if Type=='Power':
				Fields[field].append(FieldsRaw[field][t]-FieldsRaw[field][t-1])
			if Type=='Energy':
				Fields[field].append(FieldsRaw[field][t])
			Fields[field][t]=np.sum(Fields[field][t][0:DampRange[0]], axis=0)+np.sum(Fields[field][t][DampRange[1]:], axis=0)
	return Fields

	
#возвращает значение в точке от времени
def Read1DFile(File,Titles,SystemParameters,BlockSize,BufferSize):
	#FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	CompNum=len(Titles)
	ftype="f4"
	RawData = np.fromfile(File, dtype=ftype)
	RawData=RawData[int(BufferSize):]
	MaxT=int(len(RawData)/(CompNum*BlockSize))

	#data=RawData[0][j+1]
	#FieldData.append(data.reshape(( Ny, Nx)))
	print(np.shape(RawData))
	#RawData=np.swapaxes(RawData,0,1)
	print(RawData[1])
	print(len(RawData))
	#MaxT=int(len(RawData))
	print("Maxt=",MaxT)
	#RawData=RawData[:MaxT*CompNum*BlockSize]
	#RawData=RawData.reshape(MaxT,CompNum,BlockSize)
	RawData=RawData[:MaxT*CompNum*BlockSize]
	RawData=RawData.reshape(MaxT,CompNum,BlockSize)
	RawData=np.swapaxes(RawData,0,1)
	i=0
	Result={}
	for f_type in Titles:
		print(i,f_type)
		Result[f_type]=np.swapaxes(RawData[i],0,1)
		i+=1
		
	return Result,MaxT


def ReadParamFile(Path):
	files=os.listdir(Path)#получили список файлов в корневом каталоге
	#print(files)
	#считаем, что файлы параметров всегда начинаются с Param
	for i in range(0,len(files),1):
		if(files[i][0:5]=='Param'):
			ParamFile=files[i]
			
	#print('ParamFile=',ParamFile)
	fIn = open(Path+ParamFile, 'r', encoding="utf-8")#
	line = fIn.readline()
	sortNum=-1#счётчик сортов частиц
	LasersortNum=-1#счётчик сортов лазеров
	SystemParameters={}

	ParticlesParameters=collections.OrderedDict()
	LasersParameters=collections.OrderedDict()
	CurrentPartSort=''
	CurrentLaserSort=''
	ParticlesParamsTitles=['Type','DensType','DensTimeChange','Temperature','Charge','Density','Mass','LeftShift','RightShift','InitVx','PhaseMaxX_Y','PhaseMinX_Y','Width','DensProfile','Velocity']
	LasersParamsTitles=['LaserType','FocusTr','Amplitude','Omega','Tau','FocusWidth','FocusLenght']

	num=0
	while line:
		temp=line.split()
		if(temp[0][0]!='#'):
			if(temp[0]=='Particles'):
				CurrentPartSort=str(temp[1])
				ParticlesParameters[CurrentPartSort]={}
			if(temp[0]=='Laser'):
				CurrentLaserSort=str(temp[1])
				LasersParameters[CurrentLaserSort]={}
			if temp[0] in ParticlesParamsTitles:
					ParticlesParameters[CurrentPartSort][temp[0]]=temp[1:]
			if temp[0] in LasersParamsTitles:
					LasersParameters[CurrentLaserSort][temp[0]]=temp[1:]
			if temp[0] not in LasersParamsTitles and temp[0] not in ParticlesParamsTitles:
				SystemParameters[str(temp[0])]=temp[1:]
		line = fIn.readline()
		num+=1
	SystemParameters['Particles']=ParticlesParameters
	SystemParameters['Lasers']=LasersParameters
	SystemParameters['Nx']=int(SystemParameters['X_Cells'][0])
	SystemParameters['Ny']=int(SystemParameters['Y_PlasmaCells'][0])+int(SystemParameters['Y_VacCellsBottom'][0])+int(SystemParameters['Y_VacCellsTop'][0])
	if int(SystemParameters['cpu_NumOfLaserSpicies'][0])>0:
		SystemParameters['TotalLaserEnergy']=0
	for LaserSort in LasersParameters:
		print(LaserSort)
		tau=56400.*sqrt(float(SystemParameters['UniformDense'][0]))*1E-15*float(LasersParameters[LaserSort]['Tau'][0])
		print(float(SystemParameters['UniformDense'][0]),tau)
		E0=float(LasersParameters[LaserSort]['Amplitude'][0])
		sigma0=float(LasersParameters[LaserSort]['FocusWidth'][0])
		LasersParameters[LaserSort]['Energy']=0.5*E0*E0*sqrt(pi*0.5)*sigma0*0.75*tau
		SystemParameters['TotalLaserEnergy']+=LasersParameters[LaserSort]['Energy']
	#pprint.pprint(LasersParameters)

	#if SystemParameters['cpu_NumOfLaserSpicies']>0:
	#E0=18.
#sigma0=1.38
#tau=3.45378

	return SystemParameters




def ReadIntegFile(Path,SystemParameters,filename):
	FileName=Path+'/'+filename
	MaxTime=sum(1 for line in open(FileName))-2
	#print('MaxTime=',MaxTime)
	#прочесть файл Integ

	#прочесть шапку
	fIn = open(FileName, 'r', encoding="utf-8")#
	line = fIn.readline()
	dt=line.split()
	dt=float(dt[2])
	#print(dt)
	time=np.arange(0,MaxTime)*dt
	lineShapka = fIn.readline()
	#print(lineShapka)
	#удаляем лишние знаки из шапки
	lineShapka=lineShapka.replace('#','')
	lineShapka=lineShapka.replace('(','')
	lineShapka=lineShapka.replace(')','')
	#удаляем все цифры из шапки
	lineShapka = ''.join([i for i in lineShapka if not i.isdigit()])
	#print(lineShapka)
	lineShapka=lineShapka.split()
	#print('Shapka',lineShapka[1][:2])
	#создаём массив для значений энергий
	IntegEnergy={}
	for i in range(len(lineShapka)):
		#print(lineShapka[i])
		IntegEnergy[lineShapka[i]]=np.zeros([MaxTime])
		

	#print(IntegEnergy)

	#читаем данные по энергиям
	for timestep in range(0,MaxTime,1):
		line = fIn.readline()
		line=line.split()
		for column in range(0,len(line),1):
			IntegEnergy[lineShapka[column]][timestep]=line[column]

	#Ex	Ey	Ez	Bx	By	Bz	IONS	IONS_In	IONS_Out	IONS_Chg	ELECTRONS	ELECTRONS_In	ELECTRONS_Out	ELECTRONS_Chg	LEFTBEAMLEFTBEAM_In	LEFTBEAM_Out	LEFTBEAM_Chg	LeftDampEx	LeftDampEy	LeftDampEz	LeftDampB	TopDampEx	TopDampEy	TopDampEz	TopDampB	RightDampEx	RightDampEy	RightDampEz	RightDampB	BottomDampEx	BottomDampEy	BottomDampEz	BottomDampB	


			
	#print(IntegEnergy['ELECTRONS'])
	# temp=np.zeros([MaxTime])
	# tempInt=np.zeros([MaxTime])
	# #IntegEnergy['Total']=np.zeros([MaxTime])#потери энергии пучком на каждом шаге
	# #+IntegEnergy['LEFTBEAM']
	# #-IntegEnergy['LEFTBEAM_In']
	# #+IntegEnergy['LEFTBEAM_Out']
	# AllInteg=np.zeros([MaxTime])
	# for key in IntegEnergy:
		# #print(key,key.split('_')[0])
		# if(len(key.split('_'))>1):
		# #	print(len(key.split('_')),key)
			# if((key.split('_')[1]=='Chg')):
				# AllInteg+=IntegEnergy[key]
				# #print('addChg',key)

		# if(key.split('_')[0] not in SystemParameters['Particles'].keys()):
			# #print('add',key)
			# AllInteg+=IntegEnergy[key]
			
	# # AllFieldsInteg=np.zeros([MaxTime])
	# for key in IntegEnergy:
		# #print(key,key.split('_')[0])
		# #	print(len(key.split('_')),key)
		# if(key.split('_')[0] not in SystemParameters['Particles'].keys()):
			# #print('addFields',key)
			# AllFieldsInteg+=IntegEnergy[key]

	return IntegEnergy


def ReadAllIntegFiles(Path,SystemParameters,MaxTime):
	files=os.listdir(Path)#получили список файлов в корневом каталоге
	#print(files)
	tempIntegEnergy=[]
	IntegEnergy={}

	#считаем, что файлы параметров всегда начинаются с Param
	for i in range(0,len(files),1):
		if(files[i][0:5]=='Integ'):
			#IntegFiles.append(files[i])
			tempIntegEnergy.append(ReadIntegFile(Path,SystemParameters,files[i]))
	for i in range(0,len(tempIntegEnergy)):
		for key in tempIntegEnergy[i].keys():
			tempIntegEnergy[i][key]=tempIntegEnergy[i][key][0:int(MaxTime)]
	for key in SystemParameters['Particles'].keys():
		#print('key=',key)
		IntegEnergy[key]=np.zeros(MaxTime)
		IntegEnergy[key+'_In']=np.zeros(MaxTime)
		IntegEnergy[key+'_Out']=np.zeros(MaxTime)
		IntegEnergy[key+'_Chg']=np.zeros(MaxTime)
	IntegEnergy['WallDampTE']=np.zeros(MaxTime)
	IntegEnergy['WallDampTM']=np.zeros(MaxTime)
	IntegEnergy['Error']=np.zeros(MaxTime)
	#pprint.pprint(SystemParameters['Particles'].keys())
	#BottomDampOnStepBz(67) TopDampOnStepBx mpOn

	#просуммируем энергии частиц
	for i in range(0,len(tempIntegEnergy)):
		for key in tempIntegEnergy[i].keys():
			if(key.split('_')[0]  in SystemParameters['Particles'].keys()):
					IntegEnergy[key]+=tempIntegEnergy[i][key]
			if(len(key.split('_'))>1):
				if((key.split('_')[1]=='Chg')):
					IntegEnergy['Error']+=IntegEnergy[key]
	#WrongDampKeysTM=['TopDampEz','TopDampBx','TopDampBy','TopDampEx','TopDampEy','TopDampBz']
	#WallDampKeysTM=['BottomDampEz','BottomDampBx','BottomDampBy','RightDampEz','RightDampBx','RightDampBy','LeftDampEz','LeftDampBx','LeftDampBy']
	#WallDampKeysTE=['BottomDampEx','BottomDampEy','BottomDampBz','RightDampEx','RightDampEy','RightDampBz','LeftDampEx','LeftDampEy','LeftDampBz']
	WallDampKeysTE=['BottomDampEz','BottomDampBx','BottomDampBy','TopDampEz','TopDampBx','TopDampBy',]
	WallDampKeysTM=['BottomDampEx','BottomDampEy','BottomDampBz','TopDampEx','TopDampEy','TopDampBz']
	#WallDampKeysTE=['TopDampEx','TopDampEy','TopDampBz']
	#энергии полей возьмём из первого файла
	for key in tempIntegEnergy[i].keys():
		if(key.split('_')[0] not in SystemParameters['Particles'].keys() and key[-6:-2]!='Step'):
			
			if(key.split('_')[0] in WallDampKeysTM or key.split('_')[0] in WallDampKeysTE):
				print(key,key[-6:-2])
				IntegEnergy[key]=tempIntegEnergy[i][key]
				IntegEnergy['Error']+=tempIntegEnergy[i][key]
			else:
				IntegEnergy[key]=tempIntegEnergy[i][key]
				IntegEnergy['Error']+=tempIntegEnergy[i][key]
	#просуммируем поглощённое в стенках
	#WallDampKeys=['TopDampEx','TopDampEy','TopDampEz','TopDampBx','TopDampBy','TopDampBz','BottomDampEx','BottomDampEy','BottomDampEz','BottomDampBx','BottomDampBy','BottomDampBz']
	#WallDampKeysTM=['TopDampEz','TopDampBx','TopDampBy','BottomDampEz','BottomDampBx','BottomDampBy']
	#WallDampKeysTE=['TopDampEx','TopDampEy','TopDampBz','BottomDampEx','BottomDampEy','BottomDampBz']
	#WallDampKeysTM=['TopDampEz','TopDampBx','TopDampBy']
	#WallDampKeysTE=['TopDampEx','TopDampEy','TopDampBz']

	for key in IntegEnergy.keys():
		if key in WallDampKeysTE:
			IntegEnergy['WallDampTE']+=IntegEnergy[key]
			#print(IntegEnergy[key])
		if key in WallDampKeysTM:
			IntegEnergy['WallDampTM']+=IntegEnergy[key]
			#print(IntegEnergy[key])
	#pprint.pprint(IntegEnergy)
	#print('MaxTime=',MaxTime)

	WallDamp1TE=np.roll(IntegEnergy['WallDampTE'],-1)
	WallDamp1TE[0]=0
	WallDamp1TE=(WallDamp1TE-IntegEnergy['WallDampTE'])
	WallDamp1TE[-1]=0
	
	WallDamp1TM=np.roll(IntegEnergy['WallDampTM'],-1)
	WallDamp1TM[0]=0
	WallDamp1TM=(WallDamp1TM-IntegEnergy['WallDampTM'])
	WallDamp1TM[-1]=0
	BeamsPower=0
	if 'RIGHTBEAM' in SystemParameters['Particles'].keys():
		BeamsPower+=IntegEnergy['RIGHTBEAM_In'][-1]/len(IntegEnergy['RIGHTBEAM_In'])
	if 'LEFTBEAM' in SystemParameters['Particles'].keys():
		BeamsPower+=IntegEnergy['LEFTBEAM_In'][-1]/len(IntegEnergy['LEFTBEAM_In'])
	IntegEnergy['EfficiencyTE']=WallDamp1TE/BeamsPower
	IntegEnergy['EfficiencyTM']=WallDamp1TM/BeamsPower
	SystemParameters['BeamsPower']=BeamsPower
	print('BeamsPower=',BeamsPower)
	#print('IntegEnergy["Efficiency"]=',IntegEnergy['Efficiency'])

		
	return IntegEnergy
def ConvertEnergyToPower(IntegEnergy):
	WallDamp1TE=np.roll(IntegEnergy['WallDampTE'],-1)
	WallDamp1TE[0]=0
	WallDamp1TE=(WallDamp1TE-IntegEnergy['WallDampTE'])
	WallDamp1TE[-1]=0
	
	WallDamp1TM=np.roll(IntegEnergy['WallDampTM'],-1)
	WallDamp1TM[0]=0
	WallDamp1TM=(WallDamp1TM-IntegEnergy['WallDampTM'])
	WallDamp1TM[-1]=0
	IntegEnergy['WallDampTE']=WallDamp1TE
	IntegEnergy['WallDampTM']=WallDamp1TM
	
def ReadFieldsFile(WorkDir,SystemParameters,TimeStep):
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	#открыли файл
	#FieldsName=WorkDir+'Fields/DiagXY/'+'Fields2D'+str(TimeStep)
	Path=WorkDir+'Fields/DiagXY/'
	files=sorted(os.listdir(Path))#получили список файлов в корневом каталоге
	#DensName=WorkDir+'Particles/'+PartSort+'/Diag2D/'+'Diag2D'+TimeStep
	#print(files)
	FieldsName=Path+'Fields2D'+TimeStep#files[int(TimeStep)]
	print(FieldsName)
	file = open(FieldsName, 'rb')
	#прочли параметры файла
	buf = file.read(4*4)
	Prop=struct.unpack("ffff", buf[:4*4])
	Nx=int(Prop[0])
	Ny=int(Prop[1])
	size=str(int(Nx)*int(Ny))
	#print(Nx,Ny,size,SystemParameters['2D_Diag_Fields'])
	#составить формат файла
	ftype="("+str(4)+")f4"

	for f in range(0,6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			ftype+=",("+size+")f4"
	#print(ftype)
	RawData = np.fromfile(FieldsName, dtype=ftype)
	FieldData=[]

	for j in range(0, 6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			data=RawData[0][j+1]
			FieldData.append(data.reshape(( Ny, Nx)))
	#pprint.pprint(FieldData)
	result=collections.OrderedDict()
	for f in range(0,6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			result[FieldsTitles[f]]={}
			result[FieldsTitles[f]]['Type']='InTime'
			result[FieldsTitles[f]]['Sort']=FieldsTitles[f]
			result[FieldsTitles[f]]['Coord']=str(TimeStep*int(SystemParameters['Diagn_Time'][0]))
			result[FieldsTitles[f]]['data']=FieldData[f]
	#pprint.pprint(result)

	return result
	
def ReadIonFieldsFile(WorkDir,SystemParameters,TimeStep):
	FieldsTitles=['Ex','Ey','Ez','Bx','By','Bz']
	#открыли файл
	FieldsName=WorkDir+'FieldsIon/DiagXY/'+'Fields2D'+TimeStep
	file = open(FieldsName, 'rb')
	#прочли параметры файла
	buf = file.read(4*4)
	Prop=struct.unpack("ffff", buf[:4*4])
	Nx=int(Prop[0])
	Ny=int(Prop[1])
	size=str(int(Nx)*int(Ny))
	#print(Nx,Ny,size,SystemParameters['2D_Diag_Fields'])
	#составить формат файла
	ftype="("+str(4)+")f4"

	for f in range(0,6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			ftype+=",("+size+")f4"
	#print(ftype)
	RawData = np.fromfile(FieldsName, dtype=ftype)
	FieldData=[]
	for j in range(0, 6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			data=RawData[0][j+1]
			FieldData.append(data.reshape(( Ny, Nx)))
	#pprint.pprint(FieldData)
	result=collections.OrderedDict()
	for f in range(0,6):
		if(SystemParameters['2D_Diag_Fields'][f]=='1'):
			result[FieldsTitles[f]]=FieldData[f]
	#pprint.pprint(result)
	return result
	
def ReadDensFile(WorkDir,SystemParameters,PartSort,TimeStep):
	#открыли файл
	Path=WorkDir+'Particles/'+PartSort+'/Diag2D/'
	files=sorted(os.listdir(Path))#получили список файлов в корневом каталоге
	print(files)
	#DensName=WorkDir+'Particles/'+PartSort+'/Diag2D/'+'Diag2D'+TimeStep
	#print(files)
	DensName=Path+'Diag2D'+TimeStep#files[int(TimeStep)]
	print(DensName)
	file = open(DensName, 'rb')
	buf = file.read(2*4)
	Prop=struct.unpack("ff", buf[:2*4])
	Nx=int(Prop[0])
	Ny=int(Prop[1])
	size=str(int(Nx)*int(Ny))
	#print('dens',Nx,Ny,size)
	ftypeDens="("+str(2)+")f4,("+size+")f4"
	RawData = np.fromfile(DensName, dtype=ftypeDens)
	data=RawData[0][1]
	Dens={}
	Dens['data']=data.reshape(( Ny, Nx))
	Dens['Type']='InTime'
	Dens['Sort']=PartSort
	Dens['Coord']=str(TimeStep*int(SystemParameters['Diagn_Time'][0]))
	return Dens
	
def ReadPhasePtPlFile(WorkDir,SystemParameters,PartSort,TimeStep):
	#открыли файл
	Path=WorkDir+'Particles/'+PartSort+'/PhaseDiagPtPl/'
	files=sorted(os.listdir(Path))#получили список файлов в корневом каталоге
	#print(files)
	#DensName=WorkDir+'Particles/'+PartSort+'/Diag2D/'+'Diag2D'+TimeStep
	#print(files)
	DensName=Path+'PhaseDiagPtPl'+TimeStep#files[int(TimeStep)]
	print(DensName)
	file = open(DensName, 'rb')
	sz=int(SystemParameters['PhaseDiagNum'][0])
	size=str(sz*sz)
	#print('dens',Nx,Ny,size)
#	ftypePhase="("+size+")f4"
	ftypePhase="("+str(0)+")i4,("+size+")i4"

	RawData = np.fromfile(DensName, dtype=ftypePhase)
	data=RawData[0][1]
	Dens={}
	Dens['data']=data.reshape(( sz, sz))
	Dens['Type']='InTime'
	Dens['Sort']=PartSort
	Dens['Coord']=str(int(TimeStep)*int(SystemParameters['Diagn_Time'][0]))
	return Dens

def ReadPhaseFile(WorkDir,SystemParameters,PartSort,TimeStep):
	#открыли файл
	PhaseName=WorkDir+'Particles/'+PartSort+'/PhaseDiag/'+'PhaseDiag'+TimeStep
	file = open(PhaseName, 'rb')
	#прочли параметры файла
	buf = file.read(4*3)
	Prop=struct.unpack("iii", buf[:3*4])
	Npart=int(Prop[0])
	Npx=int(Prop[1])
	Ncx=int(Prop[2])
	size=str(int(Npx)*int(Ncx))
	ftypePhase="("+str(3)+")i,("+size+")i,("+size+")i"

	RawData = np.fromfile(PhaseName, dtype=ftypePhase)
	data=RawData[0][1]
	Phase_x=data.reshape(( Npx, Ncx))
	data=RawData[0][2]
	Phase_y=data.reshape(( Npx, Ncx))
	Phase={}
	Phase[PartSort]={}
	Phase[PartSort]['vx']=Phase_x
	Phase[PartSort]['vy']=Phase_y
	return Phase
